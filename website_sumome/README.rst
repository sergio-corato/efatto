===========================
SumoMe integration in Odoo
===========================

This module allows you to configure **SumoMe** in your website using your Sumo Me site ID.
SumoMe is a suite of free tools that can be used to grow your website's traffic. The SumoMe tools are easy to install and work on any website.
It allows you to connect to over 16 social services (**Facebook, Pinterest, Twitter, Google Plus** and more), consult your google analytics stats directly on your site,
increase your traffic and much more...

.. image:: https://pagely.c.presscdn.com/wp-content/uploads/2014/10/SumoMe-Social-Sharing-Buttons.png


Find below some of the features provided by SumoMe

Google Analytics Tool
================

Track your growth without leaving your site

.. raw:: html

    <section class="oe_container oe_dark"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">

.. image:: https://load.sumome.com/images/app-sections/googleanalytics-hero.png
   :width: 400px

.. raw:: html

            </div>
            <div class="oe_span6">
              <p class="oe_mt32">

Google Analytics by SumoMe makes it super easy to view your **Google Analytics** directly in the SumoMe dashboard. 

.. class:: oe\_mt32

No more digging through your data to find the numbers that matter. We highlight your most important stats so you can easily track and grow your traffic.

.. raw:: html

                  </p>
            </div>
        </div>
    </section>


Get a Clear View of How You're Doing
------------------------------------
.. raw:: html

    <section class="oe_container"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

In one glance, see exactly how your site is doing: pageviews, users, session duration, bounce rate, and more. With one click, track your 
performance today, in the past week, month, or whenever.

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/googleanalytics-section1.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>

On Page Analytics
-----------------
.. raw:: html

    <section class="oe_container oe_dark"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

See your site’s stats on each of your pages, without leaving your site or opening an app. You’ll see your stats directly on each page, 
shown only to you. Pick exactly which critical stats you see on each page.

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/googleanalytics-section2.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


Real-Time Information
---------------------
.. raw:: html

    <section class="oe_container"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

See how many people are on your site right now. See where they came from and what pages they’re all flocking to.


.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/googleanalytics-section3.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


What's Popular?
---------------
.. raw:: html

    <section class="oe_container oe_dark"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">


We show you your most popular pages, so you can get a clear view of what’s doing well and what isn’t. Use this data to do more of what 
is working and grow your traffic.

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/googleanalytics-section4.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


Heat Maps Tool
=========

.. raw:: html

    <section class="oe_container"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

Add the Heat Maps free module and Figure out Exactly How to Improve Your Site

Heat Maps shows you exactly where people are (or not) clicking on your site. With this information, you can move your call-to-actions 
to make them more visible, see what your visitors want more of, and much more.

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/heatmaps-section1.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


Keep an Eye on Every One of Your Pages
--------------------------------------
.. raw:: html

    <section class="oe_container oe_dark"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

With Heat Maps, you can track the clicks on every single one of your pages. Run unlimited campaigns to know what your visitors are doing on your homepage, content pages, and more.

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/heatmaps-section2.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>

Real-Time Feedback
------------------
.. raw:: html

    <section class="oe_container"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

Every heat map you set up is updated in real-time showing the latest clicks from your visitors. No need to wait to see where your visitors are clicking on your site!

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/heatmaps-section3.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


Share Tool
=============

Get Free Traffic To Your Site with Discover.Discover gets you more traffic. We connect you to 150,000+ other SumoMe sites to send more free traffic your way. Other SumoMe sites 
promote your content on their sites, and you promote new content for your visitors on your site. Now THAT is some sweet synergy.

Free Traffic to Your Site
-------------------------
.. raw:: html

    <section class="oe_container oe_dark"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

We figure out where your content fits best and feature your articles or sites on other SumoMe sites, driving free traffic back to you.

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/discover-section1.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


The Best Content for Your Audience
----------------------------------
.. raw:: html

    <section class="oe_container"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

With Discover on your site, you’ll never run out of good content to link your audience to. Discover determines what recommend content goes well with your site and features it at the end of your pages. This guarantees your visitors are never bored.

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/discover-section2.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


Send Traffic to Get More Traffic
--------------------------------
.. raw:: html

    <section class="oe_container oe_dark"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

Earn credits when people click on Discover links at the ends of your articles. Use those credits to get more traffic from other people’s sites.

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/discover-section3.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


Two Modes to Suit Your Fancy
----------------------------
.. raw:: html

    <section class="oe_container"> 
        <div class="oe_row oe_spaced">
            <div class="oe_span6">
              <p class="oe_mt32">

Choose between a Discover grid of 4 to 24 recommendations at the end of your article, or a scroll box that pops up with a highly-focused “read next” article. Which will you choose?

.. raw:: html

            </p>
            </div>
            <div class="oe_span6">
            <span class="oe_mt32">

.. image:: https://load.sumome.com/images/app-sections/discover-section4.png
   :width: 400px

.. raw:: html

                  </span>
            </div>
        </div>
    </section>


Configuration
=============

#. Install the module application
#. Go to http://www.sumome.com and create an account.
#. **SumoMe** will propose you to copy & paste a script in your head of you html page. It should look like :

   <script src="//load.sumome.com/" data-sumo-site-id="c28012f611dce8ab4c25f10d6b9ca9ea4b08fd7f1fb9efb56a04867e1" async="async"></script>

#. Extract the site id contained in the data-sumo-site-id attribute
#. Go in your Odoo configuration page
#. In *Configuration* click on *Website Settings*
#. Enter you site id in the form field named *SumoMe Site Id*
#. Open your home page and you should see the SumoMe toolbar. 

Developed by `Ubiteck <http://www.ubiteck.ch>`_
 

.. image:: http://www.ubiteck.ch/web/binary/company_logo?db=ubiteck&company=1



