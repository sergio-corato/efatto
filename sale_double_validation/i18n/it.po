# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 	* sale_double_validation
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 7.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-04-01 02:48+0200\n"
"PO-Revision-Date: 2016-04-01 02:52+0200\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.7\n"
"Last-Translator: \n"
"Language: it\n"

#. module: sale_double_validation
#: view:sale.double.validation.installer:0
msgid "title"
msgstr "Titolo"

#. module: sale_double_validation
#: model:ir.actions.act_window,name:sale_double_validation.action_config_sale_approval_group
msgid "Configure Approval Group for Sale "
msgstr "Configura il gruppo per l'approvazione delle vendite"

#. module: sale_double_validation
#: field:sale.double.validation.installer,group_id:0
msgid "Approval Group"
msgstr "Gruppo Approvazione"

#. module: sale_double_validation
#: help:sale.double.validation.installer,group_id:0
msgid ""
"Setting this field to a group will only allow to that group to approve Sale "
"Orders.\n"
"        Leave blank to allow any group to approve to"
msgstr ""
"Impostando questo campo su un gruppo preciso, consentirà a quel determinato "
"gruppo di approvare Preventivi e Ordini di Vendita.\n"
"        Lascia in bianco per consentire a ciascun gruppo di approvare "
"Preventivi e Ordini di Vendita"

#. module: sale_double_validation
#: view:sale.double.validation.installer:0
msgid "Sale Application Configuration"
msgstr "Configurazione Vendite"

#. module: sale_double_validation
#: view:sale.double.validation.installer:0
msgid "Configure Approval Group for Sale Orders"
msgstr "Configura Gruppo Approvazione Preventivi e Ordini di Vendita"

#. module: sale_double_validation
#: model:ir.model,name:sale_double_validation.model_sale_double_validation_installer
msgid "sale.double.validation.installer"
msgstr "sale.double.validation.installer"
