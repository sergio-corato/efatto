# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* stock_inventory_valuation_report
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-19 18:04+0000\n"
"PO-Revision-Date: 2018-03-19 18:04+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: stock_inventory_valuation_report
#: model:ir.actions.report.xml,name:stock_inventory_valuation_report.stock_inventory_valuation_report_grouped_ods
msgid "Inventory Valuation Grouped (ODS)"
msgstr "Inventario Valorizzato Raggruppato (ODS)"

#. module: stock_inventory_valuation_report
#: model:ir.actions.report.xml,name:stock_inventory_valuation_report.stock_inventory_valuation_report
msgid "Inventory Valuation per Location (ODS)"
msgstr "Inventario Valorizzato per Punto di Stoccaggio (ODS)"

