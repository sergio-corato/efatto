# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from . import sale
from . import stock_picking_package_preparation
from . import config
from . import stock
