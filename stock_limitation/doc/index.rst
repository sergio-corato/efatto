Requirements and exceptions
===========================
* After installation do not forget to assign at least one user to a location. If you left a field "Accepted Users" empy, such a location and related stocks would be visible for everybody
* Utilize the module only for internal locations
