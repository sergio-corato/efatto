# -*- coding: utf-8 -*-
#
#    Copyright (C) 2013-2014 Didotech Srl (<http://www.didotech.com>)
#    Copyright (C) 2016 Sergio Corato - SimplERP srl (<http://www.simplerp.it>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
{
    'name': 'Aeroo sale order report DEPRECATED',
    'version': '8.0.1.0.0',
    'category': 'other',
    'author': 'Sergio Corato',
    'website': 'http://www.efatto.it',
    'description': 'Aeroo sale order report',
    'license': 'AGPL-3',
    'depends': [
        'account',
        'report_aeroo',
        'report_branding',
        'sale',
        'sale_order_report_hide_prices',
        'report_aeroo_parser',
    ],
    'data': [
        'data/reports.xml',
    ],
    'installable': True
}
