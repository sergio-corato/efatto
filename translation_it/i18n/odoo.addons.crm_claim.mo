��    x      �  �   �      (
     )
     2
     ;
  	   G
  ~  Q
  D  �  z       �     �     �     �     �  
   �     �     �     �  
   �               .     :     I     U     b     p     }     �     �  
   �     �     �     �     �  
   �     �        
     
        "     0  .   <     k     q     z     �  $   �     �     �     �  	   �  	   �     �  e         f  �   k     �  /   �  �   $     �     �     �     �     �  k   �     a     e  "   n     �  
   �  
   �     �     �     �     �  
   �     �      �          /     7     =  
   K     V     h  	   q     {  
   �     �     �     �     �  S   �  
   $     /  
   ;     F     R     Y     g     o     x     �     �  
   �     �     �  �   �     R     f     k     }     �  &   �     �     �     �     �     �       �    	   �     �     �  
   �  �  �  
  �  /  �          /     ;     B     I  	   \  	   f     p     x     �     �     �     �     �     �     �               )     D     L     \     j     q     z     �  
   �     �     �  	   �  	   �     �     �  4   �     .     3     <     Q  +   ]     �     �     �  	   �  	   �     �  s   �     D   �   I      �   ;   �   �   !     �!     �!     �!     �!     �!  r   �!     o"     u"      ~"     �"     �"     �"     �"     �"     �"     �"     #     #  %   '#     M#     _#     g#     p#  
   �#     �#  	   �#     �#  	   �#     �#     �#     �#     �#     �#  m   $     |$     �$     �$     �$     �$     �$     �$     �$     �$     �$     �$  	   �$      %  	   %  �   %     �%     �%     �%     �%     &  A   &     Y&     `&     o&     �&     �&     �&     :   w   e       N      `   3   %          G   H   -                  \             f   I   *   t   &   ^                    Y       o   m   P       +   a   ?   )   s   R          E   M   W                         g          J   #   i   6   <          ]       8       
       Q   [   p   @          x   ,          _   	          d   D       l   L   b   V       9   0   '   B   T      k   j           S      1   u   (   4   "       X   7          q       K       >   O   2   F      =   h       C   A       /                       .   v           n          c           Z   ;          r   !           U      5   $           # Claims # Emails # of Claims %s (copy) <p class="oe_view_nocontent_create">
                    Record and track your customers' claims. Claims may be linked to a sales order or a lot.You can send emails with attachments and keep the full history for a claim (emails sent, intervention type and so on).Claims may automatically be linked to an email address using the mail gateway module.
                </p>
             <p class="oe_view_nocontent_create">
                Click to create a claim category.
              </p><p>
                Create claim categories to better manage and classify your
                claims. Some example of claims can be: preventive action,
                corrective action.
              </p>
             <p class="oe_view_nocontent_create">
                Click to setup a new stage in the processing of the claims. 
              </p><p>
                You can create claim stages to categorize the status of every
                claim entered in the system. The stages define all the steps
                required for the resolution of a claim.
              </p>
             Action Description... Action Type Actions Active CRM Claim Report Categories Category Claim Claim Categories Claim Date Claim Date by Month Claim Description Claim Month Claim Reporter Claim Stage Claim Stages Claim Subject Claim stages Claim/Action Description Claims Claims Analysis Close Date Closed Closure Common to All Teams Company Corrective Corrective Action Create Date Created by Created on Creation Date Date Closed Date of the last message posted on the record. Dates Deadline Delay to close Description Destination email for email gateway. Email Extended Filters... Factual Claims Follow Up Followers Group By Have a general overview of all claims processed in the system by sorting them with specific criteria. High Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. ID If checked new messages require your attention. If you check this field, this stage will be proposed by default on each sales team. It will not assign this stage to existing teams. In Progress Is a Follower Last Message Date Last Updated by Last Updated on Link between stages and sales teams. When set, this limitate the current stage to the selected sales teams. Low Messages Messages and communication history Month of claim My Case(s) My Company My Sales Team(s) New Next Action Next Action Date No Subject Normal Number of Days to close the case Overpassed Deadline Partner Phone Policy Claims Preventive Preventive Action Priority Reference Rejected Resolution Resolution Actions Responsibilities Responsible Responsible User Responsible sales team. Define Responsible user and Email account for mail gateway. Root Cause Root Causes Sales Team Salesperson Search Search Claims Section Sections Sequence Settled Stage Stage Name Stages Summary These email addresses will be added to the CC field of all inbound and outbound emails for this record before being sent. Separate multiple email addresses with a comma Trouble Responsible Type Unassigned Claims Unread Messages Update Date Used to order stages. Lower is better. User Value Claims Watchers Emails Website Messages Website communication history Workload Project-Id-Version: Odoo 8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-01-21 14:07+0000
PO-Revision-Date: 2016-05-08 19:03+0200
Last-Translator: Martin Trigaux
Language-Team: Italian (http://www.transifex.com/odoo/odoo-8/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7
 # Reclami # Email # di reclami %s (copia) <p class="oe_view_nocontent_create">
                  Inserisci e Traccia i Reclami dei Clienti.
                  I reclami possono essere collegati a ordini di ventia o ad un lotto di produzione. 
                  A partire dall'invio di una mail è possibile tenere traccia della completa storia di un reclamo (messaggi inviati, tipi di interventi ecc...) .
                  I reclami possono essere automaticamente collegati ad un indirizzo e-mail utilizzando il modulo Mail Gateway.
</p>
             <p class="oe_view_nocontent_create">
Click per creare una categoria di reclamo.
</p><p>
Crea una categoria di raclamo per gestire meglio e classificare i propri
reclami. Alcuni esempi di reclamo possono essere: azioni preventive, 
azioni correttive.</p>
             <p class="oe_view_nocontent_create">
Click per importare un nuovo stage nel processo per i reclami.
</p><p>
Potete creare uno stage di reclamo per categorizzare lo stato di ogni
reclamo inserito nel sistema. Gli stage definiscono tutti i passi
richiesti per una risoluzione del reclamo.</p>
             Descrizione azione... Tipo azione Azioni Attivo Report dei reclami Categorie Categoria Reclami Categorie reclami Data reclamo Data reclamo per mese Descrizione del reclamo Mese reclamo Segnalatore del Reclamo Fase reclamo Fasi del Reclamo Oggetto del reclamo Fasi reclamo Descrizione reclamo/azione Reclami Analisi reclami Data chiusura Chiuso Chiusura Comune a Tutti i Teams Azienda Correttivo Azione correttiva Data creazione Creato da Creato il Data creazione Data di chiusura Data dell'ultimo messaggio postato per questo record Date Scadenza Ritardo per Chiusura Descrizione Email di destinazione per il gateway email. Email Filtri estesi... Reclami reali Follow Up Followers Raggruppa per Per avere una visione generale di tutti i reclami processati nel sistema ordinando gli stesso per specifici criteri Alto Gestisce il sommario (numero di messaggi, ...) di Chatter. Questo sommario è direttamente in html così da poter essere inserito nelle viste kanban. ID Se selezionato, nuovi messaggi richiedono la tua attenzione Se si abilita questo campo, questa fase verrà proposta automaticamente per ogni team di vendita. Non verrà assegnata ai team esistenti. In corso E' un Follower Data Ultimo Messaggio Ultima modifica di Ultima modifica il Collegamento tra fasi e team di vendita. Quando impostato, limita la fase corrente al team di vendita selezionato. Basso Messaggi Storico messaggi e comunicazioni Mese del reclamo I miei casi(o) La mia azienda I Miei Team di Vendita Nuovo Prossima Azione Data prossima azione Nessun Oggetto Normale Numero di giorni per chiudere il caso Scadenza superata Partner Telefono Politiche reclami Preventivo Azione preventiva Priorità Riferimento Rifiutato Risoluzione Azione risolutiva Responsabilità Responsabile Utente Responsabile Responsabile del team delle vendite. Definire l'utente responsabile ed un account email per il gateway email. Causa principale Cause principali Team di vendita Commerciale Ricerca Cerca reclami Sezione Sezioni Sequenza Risolto Fase Nome Fase Fasi Riepilogo Questi indirizzi email verranno aggiunti nel campo CC di tutte le email, in entrate e uscita, prima di essere spedite. E' necessario separare gli indirizzi con una virgola Responsabile Problematiche Tipo Reclami Non Assegnati Messaggi Non Letti Data Aggiornamento Usato per ordinare le fasi. Più basso significa più importante. Utente Valore reclami Email osservatori Messaggi sito Storico comunicazione sito Carico di lavoro 