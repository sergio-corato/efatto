[![Build Status](https://travis-ci.org/sergiocorato/__librerp__.svg?branch=8.0)](https://travis-ci.org/sergiocorato/__librerp__)
[![Coverage Status](https://coveralls.io/repos/sergiocorato/__librerp__/badge.svg?branch=8.0&service=github)](https://coveralls.io/github/sergiocorato/__librerp__?branch=8.0)
![Alt License](https://img.shields.io/badge/licence-AGPL--3-blue.svg) 


EFATTO.IT
====================================

Customized version of Odoo for Italy


Contributors
------------

* Sergio Corato <sergiocorato@gmail.com>

Maintainer
----------

![Alt Efatto.it](http://efatto.it/images/Logo.png "http://efatto.it") 

Efatto
=======================

http://efatto.it

This module is maintained by Efatto.it.

Efatto.it promotes a customized Odoo with additional features for Italian usage.

To contribute to this module, please visit http://efatto.it.
