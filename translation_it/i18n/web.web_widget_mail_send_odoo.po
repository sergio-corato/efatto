# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* web_widget_mail_send_odoo
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-17 19:44+0100\n"
"PO-Revision-Date: 2016-03-17 19:45+0100\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.7\n"
"Last-Translator: \n"
"Language: it\n"

#. module: web_widget_mail_send_odoo
#. openerp-web
#: code:addons/web_widget_mail_send_odoo/static/src/js/web_widget_mail_send_odoo.js:33
#, python-format
msgid "Can't send email to invalid e-mail address"
msgstr "Non è possibile inviare mail ad indirizzi non corretti"

#. module: web_widget_mail_send_odoo
#. openerp-web
#: code:addons/web_widget_mail_send_odoo/static/src/js/web_widget_mail_send_odoo.js:33
#, python-format
msgid "E-mail Error"
msgstr "Errore Email"

#. module: web_widget_mail_send_odoo
#. openerp-web
#: code:addons/web_widget_mail_send_odoo/static/src/js/web_widget_mail_send_odoo.js:75
#, python-format
msgid "Please complete partner's information."
msgstr "Devi prima compilare le informazioni relative al partner."
