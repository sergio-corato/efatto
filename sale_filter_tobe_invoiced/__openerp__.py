# -*- coding: utf-8 -*-
# © 2015 Akretion (http://www.akretion.com)
# Sébastien BEAU <sebastien.beau@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Sale filter order to be invoiced",
    "version": "8.0.0.1.0",
    "category": "Uncategorized",
    "website": "https://akretion.com",
    "author": "Akretion",
    "license": "AGPL-3",
    "installable": True,
    "depends": [
        "sale",
    ],
    "data": [
        "views/sale_view.xml",
    ],
}
