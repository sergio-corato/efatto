# -*- coding: utf-8 -*-
# Copyright 2019 Sergio Corato
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).
{
    'name': "Single tax restrict in invoice",
    'version': "8.0.1.0.0",
    "author": "Sergio Corato",
    'website': "https://efatto.it",
    'category': "Localisation / Accounting",
    'license': "LGPL-3",
    'depends': ["account"],
    'data': [
    ],
    'installable': True,
}
